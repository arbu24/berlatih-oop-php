<?php
// Release 0
require 'animal.php';
require 'ape.php';
require 'frog.php';

$sheep = new Animal("shaun");

echo 'Nama hewan : '.$sheep->name.'<br>';
echo 'Jumlah kaki : '.$sheep->legs.'<br>';
echo 'Apakah merupakan hewan berdarah dingin ? '.$sheep->cold_blooded.'<br>';
echo '<br>';
// var_dump($sheep);

// Release 1
$sungokong = new Ape("kera sakti");

echo 'Nama hewan : '.$sungokong->name.'<br>';
echo 'Jumlah kaki : '.$sungokong->legs.'<br>';
echo 'Apakah merupakan hewan berdarah dingin ? '.$sungokong->cold_blooded.'<br>';
echo 'Karakteristik hewan ini adalah : '; $sungokong->yell();

// var_dump($sungokong);

echo '<br><br>';

$kodok = new Frog("buduk");
echo 'Nama hewan : '.$kodok->name.'<br>';
echo 'Jumlah kaki : '.$kodok->legs.'<br>';
echo 'Apakah merupakan hewan berdarah dingin ? '.$kodok->cold_blooded.'<br>';
echo 'Karakteristik hewan ini adalah : '; $kodok->jump();

// var_dump($kodok);
?>
