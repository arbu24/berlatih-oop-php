<?php

class Kendaraan {
  public $merk;
  public $warna;
  public function __construct($merk = 'honda', $warna = 'putih') 
  {
    $this->merk = $merk;
    $this->warna = $warna;
  }
}

$kendaraan1 = new Kendaraan("Senia");

echo $kendaraan1->merk."<br>";
echo $kendaraan1->warna."<br>";


class mobil extends Kendaraan{
    
}

$mobil1 = new mobil();

$mobil1->merk = 'Daihatsu';
$mobil1->warna = 'merah';

echo $mobil1->merk;
echo $mobil1->warna;

?>